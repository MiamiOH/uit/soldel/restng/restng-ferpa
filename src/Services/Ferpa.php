<?php
/*
-----------------------------------------------------------
FILE NAME: Ferpa.class.php

Copyright (c) 2018 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Meenakshi Kandasamy

DESCRIPTION:  Class for the Ferpa data RESTng Service

ENVIRONMENT DEPENDENCIES: RESTng

AUDIT TRAIL:

DATE        UniqueID
05/7/2018  Kandasm       Initial File
*/

namespace MiamiOH\RestngFerpa\Services;


use Respect\Validation\Rules\Uppercase;

class Ferpa extends \MiamiOH\RESTng\Service
{

    private $dbDataSourceName = 'MUWS_GEN_PROD';
    private $dbh;
    private $bannerUtil;

    public function setBannerUtil($bannerUtil)
    {
        /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    public function getFerpaAuthorizations()
    {
        $payload = [];
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();


        //$this->dbh = $this->database->getHandle($this->datasource_name);

        $studentId[] = $request->getResourceParam('muid');

        if ($request->getOptions('muid') !== [] && $request->getResourceParam('muid') == []) {
            $studentId = $options['uniqueid'];
        }

        $keyField = 'uniqueId';
        //validation
        switch ($request->getResourceParamKey('muid')) {
            case 'uniqueId':
                $keyField = 'uniqueId';
                $this->validateInput($studentId, $this->getPattern("uniqueId"), "Invalid unique ID.");
                break;
            case 'pidm':
                $keyField = 'pidm';
                $this->validateInput($studentId, $this->getPattern("pidm"), "Invalid pidm.");
                break;
        }
        //make sure uid exists
        if ($studentId === null) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        if ($keyField === 'uniqueId') {
            try {
                $pidm = $this->getPidm($studentId);
            } catch (\Exception $e) {
                throw new \MiamiOH\RESTng\Exception\BadRequest($request->getResourceParamKey('muid') . " does not exist.");
            }
        } else {
            $pidm = $studentId;
        }

//        if ($pidm === DB_EMPTY_SET) {
//            throw new \Exception('Invalid Unique ID provided.');
//        }

//        if (empty($pidm)) {
//            throw new \Exception('Invalid Unique ID provided.');
//        }

        foreach ($pidm as $value) {
            $value = $value['szbuniq_pidm'];
            $payload[] = $this->buildCompleteAuths($this->getStudentInfo($value), $this->getAuthorizations($value, 'N'),
                $this->getAuthorizations($value, 'Y'));
        }

        if (count($payload) <= 0) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        // Response was successful and Return information
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;
    }

    public function buildCompleteAuths($studentInfo, $completeAuths, $inCompleteAuths)
    {
        $records = array();
        foreach ($studentInfo as $row) {
            $records['name'] = $row['spriden_first_name'];
            $records['lastName'] = $row['spriden_last_name'];
            $records['uniqueId'] = $row['szbuniq_unique_id'];
            $records['bannerId'] = $row['spriden_id'];
            $records['currentUser'] = true;
        }

        $records['completeAuths'] = $this->getAuthPayload($completeAuths);
        $records['inCompleteAuths'] = $this->getAuthPayload($inCompleteAuths);


        return $records;
    }

    public function getAuthPayload($auths)
    {
        $allAuthRecords = array();
        if (!empty($auths)) {
            foreach ($auths as $auth) {
                $authRecord[$auth['family_member_id']] = array(
                    'name' => $auth['name'],
                    'emailAddress' => $auth['email'],
                    'partBAuth' => $auth['academic_year'] !== 'Other' ? true : false,
                    'accounts' => $auth['bills_auth'] === 'Y' ? true : false,
                    'courses' => $auth['course_auth'] === 'Y' ? true : false,
                    'financialAids' => $auth['fin_aid_auth'] === 'Y' ? true : false,
                    'grades' => $auth['grades_auth'] === 'Y' ? true : false,
                    'hdgs' => $auth['hdgs_auth'] === 'Y' ? true : false,
                    'personalInfos' => $auth['personal_info_auth'] === 'Y' ? true : false
                );
                $allAuthRecords = $authRecord;
            }
        }
        return $allAuthRecords;
    }

    public function getStudentInfo($pidm)
    {
        $query = "select szbuniq_unique_id,spriden_id,spriden_first_name,spriden_last_name from spriden,szbuniq where spriden_change_ind is null and spriden_pidm = szbuniq_pidm and spriden_pidm = $pidm";
        return $this->dbh->queryall_array($query);
    }

    public function getAuthorizations($pidm, $pendingStatus)
    {
        $isPendingNull = '';

        if ($pendingStatus === 'N') {
            $isPendingNull = 'OR pending is null';
        }

        $query = "select
                    student_pidm,
                    MM4f_authorizations2.family_member_id as family_member_id,
                    first_name ||' '|| middle_name ||' '|| last_name as name,
                    email,
                    academic_year,
                    relationship_code,
                    personal_info_auth,
                    course_auth ,
                    grades_auth,
                    bills_auth ,
                    fin_aid_auth,
                    hdgs_auth,
                    auth_comment,
                    pending from safmgr.MM4f_authorizations2,safmgr.MM4f_family_member_info
                    where student_pidm = $pidm
                    and MM4f_authorizations2.family_member_id =MM4f_family_member_info.family_member_id
                    and (pending ='$pendingStatus' $isPendingNull)";
        return $this->dbh->queryall_array($query);
    }

    //Get regex patterns for input params
    protected function getPattern($val)
    {
        if ($val == 'pidm') {
            return '/^\d{1,8}$/';
        } else {
            if ($val == 'uniqueId') {
                return '/^\w{1,8}$/';
            }
        }
    }

    //validate input params based on regex
    protected function validateInput($input, $pattern, $errMesg)
    {
        if (is_array($input)) {
            foreach ($input as $value) {
                if (!preg_match($pattern, $value)) {
                    throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
                }
            }
        } else {
            if (!preg_match($pattern, $input)) {
                throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
            }
        }
    }

    protected function getPidm($uniqueId)
    {

        $uniqueIdString = implode("','", $uniqueId);
        $uniqueIdString = strtoupper("'" . $uniqueIdString . "'");
        $query = "select szbuniq_pidm from szbuniq where szbuniq_unique_id IN ($uniqueIdString)";
        return $this->dbh->queryall_array($query);
    }

}
