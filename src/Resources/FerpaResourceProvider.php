<?php

namespace MiamiOH\RestngFerpa\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class FerpaResourceProvider extends ResourceProvider
{

    private $tag = "Ferpa";
    private $dot_path = "Ferpa.Ferpa";
    private $s_path = "/ferpa/v3";
    private $bs_path = '\Ferpa';

    public function registerDefinitions(): void
    {
//        $this->addTag(array(
//    'name' => 'Ferpa',
//    'description' => 'Ferpa resources'
//));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Ferpa',
            'type' => 'object',
            'properties' => array(
                'firstName' => array('type' => 'string', 'description' => 'student first name'),
                'lastName' => array('type' => 'string', 'description' => 'student last name'),
                'uniqueId' => array('type' => 'string', 'description' => 'student uniqueId'),
                'bannerId' => array('type' => 'string', 'description' => 'student bannerId'),
                'currentUser' => array('type' => 'string', 'description' => 'current user'),
                'completeAuth' => array(
                    'familMemberId' => array(
                        'name' => array('type' => 'string', 'description' => 'Family member name'),
                        'emailAddress' => array('type' => 'string', 'description' => 'Family member email address'),
                        'partBAuth' => array('type' => 'string', 'description' => 'partB auth'),
                        'accounts' => array('type' => 'string', 'description' => 'Family member first name'),
                        'courses' => array('type' => 'string', 'description' => 'Family member first name'),
                        'financialAids' => array('type' => 'string', 'description' => 'Family member first name'),
                        'grades' => array('type' => 'string', 'description' => 'Family member first name'),
                        'hdgs' => array('type' => 'string', 'description' => 'Family member first name'),
                        'personalInfos' => array('type' => 'string', 'description' => 'Family member first name'),
                    )
                ),
                'inCompleteAuth' => array(
                    'familMemberId' => array(
                        'name' => array('type' => 'string', 'description' => 'Family member first name'),
                        'emailAddress' => array('type' => 'string', 'description' => 'Family member email address'),
                        'partBAuth' => array('type' => 'string', 'description' => 'partB auth'),
                        'accounts' => array('type' => 'string', 'description' => 'Family member first name'),
                        'courses' => array('type' => 'string', 'description' => 'Family member first name'),
                        'financialAids' => array('type' => 'string', 'description' => 'Family member first name'),
                        'grades' => array('type' => 'string', 'description' => 'Family member first name'),
                        'hdgs' => array('type' => 'string', 'description' => 'Family member first name'),
                        'personalInfos' => array('type' => 'string', 'description' => 'Family member first name'),
                    )
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Ferpa.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Ferpa',
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Ferpa',
            'class' => 'MiamiOH\RestngFerpa\Services' . $this->bs_path,
            'description' => 'This service provides details about what access student gave to familymembers',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
//        'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read', //GET
                'name' => $this->dot_path . '.get',
                'description' => 'Return ferpa authorization information',
                'pattern' => $this->s_path . '/:muid',
                'service' => 'Ferpa',
                'method' => 'getFerpaAuthorizations',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'params' => array(
                    'muid' => array(
                        'description' => 'Student UniqueId or Pidm.',
                        'alternateKeys' => ['uniqueId', 'pidm']
                    ),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Collection of ferpa authorization information',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Ferpa.Collection',
                        )
                    ),
                ),
                'middleware' => array(
                    'authorize' => [
                        'application' => 'WebServices',
                        'module' => 'Ferpa',
                        'key' => 'admin'
                    ],
                    'authenticate' => array(
                        array(
                            'type' => 'token'
                        ),
                    ),
                )
            )
        );

        $this->addResource(array(
                'action' => 'read', //GET
                'name' => $this->dot_path . '.getByList',
                'description' => 'Return collection of ferpa authorization information based on uniqueid\'s. ',
                'summary' => 'Get a collection of ferpa information',
                'pattern' => $this->s_path,
                'service' => 'Ferpa',
                'method' => 'getFerpaAuthorizations',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'params' => [],
                'options' => [
                    'uniqueid' => [
                        'description' => 'list of uniqueid\'s seperated by \',\'' ,
                        'type' => 'list'
                    ],
                ],
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Collection of ferpa authorization information',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Ferpa.Collection',
                        )
                    ),
                ),
                'middleware' => array(
                    'authorize' => [
                        'application' => 'WebServices',
                        'module' => 'Ferpa',
                        'key' => 'admin'
                    ],
                    'authenticate' => array(
                        array(
                            'type' => 'token'
                        ),
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}