<?php

namespace MiamiOH\RestngFerpa\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: getFerpaTest.php

Copyright (c) 2018 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Meenakshi Kandasamy

DESCRIPTION:  Unit Tests for Testing the GET Functionality of
              the ferpa authentication Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID

*/

class GetFerpaTest extends \MiamiOH\RESTng\Testing\TestCase
{


    /*************************/
    /**********Set Up*********/
    /*************************/

    private $dbh, $request, $response, $bannerUtil, $ferpa, $queryallRecords;

    protected function setUp()
    {

        $api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'callResource'))
            ->getMock();

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array(
                'getResourceParam',
                'getOptions',
                'getSubObjects',
                'getResourceParamKey',
                'getOffset',
                'getLimit'
            ))
            ->getMock();


        //set up the mock dbh
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'queryfirstcolumn'))
            ->getMock();


        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();


        $this->ferpa = new \MiamiOH\RestngFerpa\Services\Ferpa();

        $this->ferpa->setDatabase($db);
        $this->ferpa->setBannerUtil($this->bannerUtil);
        $this->ferpa->setRequest($this->request);

    }

//

    /**
     * Mock Options for Unique ID Single Job Test
     */
    public function mockOptionsUniqueID()
    {
        return array('uniqueId' => ['kandasm']);
    }

    public function testferpaStudentInformation()
    {

//        $this->request->method('getOptions')
//            ->will($this->returnCallback(array($this, 'mockOptionsUniqueID')));
//
//
        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockStudentInfoQuery'
            )));

        $studentInfo = $this->ferpa->getStudentInfo('11111');

        $this->assertEquals($this->mockStudentInfoQuery(), $studentInfo);

        $this->assertEquals('John', $studentInfo[0]['spriden_first_name']);
        // $this->assertEquals($payload, $this->returnferpaSingle());

    }


    public function testferpaAuthInformation()
    {
        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockApprovedAuthInformation'
            )));

        $authInfo = $this->ferpa->getAuthorizations('890915', 'N');

        $this->assertEquals($this->mockApprovedAuthInformation(), $authInfo);

        $this->assertEquals('chris@email.com', $authInfo[0]['email']);
    }



    // public function testExceptionRaisedForInvalidUniqueId(){
    //     $this->request->method('getResourceParam')
    //         ->with('muid')->willReturn('kandafdfd');
    //     $this->request->method('getResourceParamKey')
    //         ->with('muid')
    //         ->willReturn('uniqueId');
    //     // $this->setExpectedException('MiamiOH\RESTng\Exception\BadRequest');
    //     try {
    //         $this->response = $this->ferpa->getFerpaAuthorizations();
    //         $this->fail("Expected exception has not been raised.");
    //
    //     } catch (Exception $ex) {
    //         $this->assertEquals($ex->getMessage(), "Invalid unique ID.");
    //     }
    // }

    public function testferpaAuthorizations()
    {
        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('uniqueId');
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('kandasm');

        $this->dbh->method('queryall_array')->will(
            $this->onConsecutiveCalls(
                $this->returnCallback(array(
                        $this,
                        'mockPidm'
                    )
                ),
                $this->returnCallback(array(
                        $this,
                        'mockStudentInfoQuery'
                    )
                ),
                $this->returnCallback(array(
                        $this,
                        'mockApprovedAuthInformation'
                    )
                ), $this->returnCallback(array(
                    $this,
                    'mockPendingAuthInformation'
                )
            ))
        );

        $this->response = $this->ferpa->getFerpaAuthorizations();
        $payload = $this->response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->mockAllFerpaInfo());
    }

    public function mockPidm()
    {
        return array
        (
            array
            (
                'szbuniq_pidm' => 890915
            )
        );
    }

    public function mockStudentInfoQuery()
    {
        return array
        (
            array
            (
                'szbuniq_unique_id' => 'DoeJ',
                'spriden_id' => '+00882600',
                'spriden_first_name' => 'John',
                'spriden_last_name' => 'Doe'
            )
        );
    }

    public function mockApprovedAuthInformation()
    {
        return array(
            array(
                'student_pidm' => '890915',
                'family_member_id' => '226571',
                'name' => 'Chris Jockson',
                'email' => 'chris@email.com',
                'academic_year' => 'Other',
                'relationship_code' => 'F',
                'personal_info_auth' => 'N',
                'course_auth' => 'N',
                'grades_auth' => 'N',
                'bills_auth' => 'N',
                'fin_aid_auth' => 'N',
                'hdgs_auth' => 'Y',
                'auth_comment' => '',
                'pending' => 'N'
            ),
        );
    }

    public function mockPendingAuthInformation()
    {
        return array(
            array(
                'student_pidm' => '890915',
                'family_member_id' => '111111',
                'name' => 'John Doe',
                'email' => 'doe@gmail.com',
                'academic_year' => 'Other',
                'relationship_code' => 'F',
                'personal_info_auth' => 'N',
                'course_auth' => 'N',
                'grades_auth' => 'N',
                'bills_auth' => 'N',
                'fin_aid_auth' => 'N',
                'hdgs_auth' => 'N',
                'auth_comment' => '',
                'pending' => 'Y'
            ),
        );
    }

    public function mockAllFerpaInfo()
    {
        return array
        (
            array
            (
                'name' => 'John',
                'lastName' => 'Doe',
                'uniqueId' => 'DoeJ',
                'bannerId' => '+00882600',
                'currentUser' => true,
                'completeAuths' => array
                (
                    '226571' => array
                    (
                        'name' => 'Chris Jockson',
                        'emailAddress' => 'chris@email.com',
                        'partBAuth' => '',
                        'accounts' => '',
                        'courses' => '',
                        'financialAids' => '',
                        'grades' => '',
                        'hdgs' => 1,
                        'personalInfos' => '',
                    )

                ),
                'inCompleteAuths' => array
                (
                    '111111' => Array
                    (
                        'name' => 'John Doe',
                        'emailAddress' => 'doe@gmail.com',
                        'partBAuth' => '',
                        'accounts' => '',
                        'courses' => '',
                        'financialAids' => '',
                        'grades' => '',
                        'hdgs' => '',
                        'personalInfos' => ''
                    )
                )
            )
        );
    }

    /**
     * Mock Database Return for Single ferpa Test
     */
    public function mockQuerySingle()
    {
        return array(
            array(
                "name" => "John",
                "lastName" => "Doe",
                "uniqueId" => "DoeJ",
                "bannerId" => "+00882600",
                "currentUser" => true,
                "completeAuths" => array(
                    "226571" => array(
                        "name" => "dWDA  EFAEF",
                        "emailAddress" => "vaoiaos@d.c",
                        "partBAuth" => false,
                        "accounts" => false,
                        "courses" => false,
                        "financialAids" => false,
                        "grades" => false,
                        "hdgs" => true,
                        "personalInfos" => false
                    ),
                ),
                "inCompleteAuths" => array(
                    "111111" => array(
                        "name" => "John Doe",
                        "emailAddress" => "doe@gmail.com",
                        "partBAuth" => false,
                        "accounts" => false,
                        "courses" => false,
                        "financialAids" => false,
                        "grades" => false,
                        "hdgs" => false,
                        "personalInfos" => false
                    )
                )
            )
        );
    }

    /**
     * Expected Return from Unique ID Single ferpa Test
     */
    public function returnferpaSingle()
    {
        return array(
            array(
                "name" => "John",
                "lastName" => "Kandasamy",
                "uniqueId" => "KANDASM",
                "bannerId" => "+00882600",
                "currentUser" => true,
                "completeAuths" => array(
                    "226549" => array(
                        "name" => "fsdf  fdasf",
                        "emailAddress" => "don.k1dd@gmail.com",
                        "partBAuth" => false,
                        "accounts" => false,
                        "courses" => false,
                        "financialAids" => false,
                        "grades" => false,
                        "hdgs" => false,
                        "personalInfos" => false
                    ),
                ),
                "inCompleteAuths" => array()
            )
        );
    }


    public function testInvalidUniqueId()
    {
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('fedsd$54');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('uniqueId');

        // $this->setExpectedException('MiamiOH\RESTng\Exception\BadRequest');
        try {
            $this->response = $this->ferpa->getFerpaAuthorizations();
            $this->fail("Expected exception has not been raised.");

        } catch (\Exception $ex) {
            $this->assertEquals($ex->getMessage(), "Invalid unique ID.");
        }
    }

    public function testInvalidPidm()
    {
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('&^123$@*');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('pidm');

        // $this->setExpectedException('MiamiOH\RESTng\Exception\BadRequest');
        try {
            $this->response = $this->ferpa->getFerpaAuthorizations();
            $this->fail("Expected exception has not been raised.");

        } catch (\Exception $ex) {
            $this->assertEquals($ex->getMessage(), "Invalid pidm.");
        }

    }

}
